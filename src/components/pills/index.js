import pill_crashboxx from "./pill_crashboxx.vue";
import pill_mapping from "./pill_mapping.vue";
import pill_analytics from "./pill_analytics.vue";
import pill_fleet_mgnt from "./pill_fleet_mgnt.vue";
import pill_routing from "./pill_routing.vue";
import pill_control_tower from "./pill_control_tower.vue";
import pill_live_track from "./pill_live_track.vue";
import pill_shipments from "./pill_shipments.vue";
import pill_back_to_launcher from "./pill_back_to_launcher.vue";
export default {
  pill_crashboxx,
  pill_mapping,
  pill_analytics,
  pill_fleet_mgnt,
  pill_routing,
  pill_control_tower,
  pill_live_track,
  pill_shipments,
  pill_back_to_launcher,
};
