import FlotasIso from "./flotas_iso.vue";
import LeapsightIso from "./leapsight_iso.vue";
import LeapsightIsologo from "./leapsight_isologo.vue";
import MagentaIsoMonocrome from "./magenta_iso_monocrome.vue";
import MagentaIsologo from "./magenta_isologo.vue";
import MagentaLogoMWhite from "./magenta_logo_m_white.vue";
import StrixLogo from "./strix_logo.vue";
import StrixLogoWhite from "./strix_logo_white.vue";
import StrixSLogo from "./strix_s_logo.vue";

export default {
  FlotasIso,
  LeapsightIso,
  LeapsightIsologo,
  MagentaIsoMonocrome,
  MagentaIsologo,
  MagentaLogoMWhite,
  StrixLogo,
  StrixLogoWhite,
  StrixSLogo,
};
