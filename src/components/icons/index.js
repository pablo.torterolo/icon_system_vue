import icon_account_settings from "./icon_account_settings.vue";
import icon_add from "./icon_add.vue";
import icon_add_fill from "./icon_add_fill.vue";
import icon_agents from "./icon_agents.vue";
import icon_alarma from "./icon_alarma.vue";
import icon_analytics from "./icon_analytics.vue";
import icon_arrow_back from "./icon_arrow_back.vue";
import icon_arrow_course from "./icon_arrow_course.vue";
import icon_arrow_down from "./icon_arrow_down.vue";
import icon_arrow_drop_down from "./icon_arrow_drop_down.vue";
import icon_arrow_drop_up from "./icon_arrow_drop_up.vue";
import icon_arrow_forward from "./icon_arrow_forward.vue";
import icon_arrow_up from "./icon_arrow_up.vue";
import icon_attention from "./icon_attention.vue";
import icon_bag from "./icon_bag.vue";
import icon_bell from "./icon_bell.vue";
import icon_bookmark from "./icon_bookmark.vue";
import icon_bookmark_outline from "./icon_bookmark_outline.vue";
import icon_brujula from "./icon_brujula.vue";
import icon_brujula_vacia from "./icon_brujula_vacia.vue";
import icon_calendar from "./icon_calendar.vue";
import icon_camera from "./icon_camera.vue";
import icon_car from "./icon_car.vue";
import icon_car_battery from "./icon_car_battery.vue";
import icon_car_brakes from "./icon_car_brakes.vue";
import icon_car_bujia from "./icon_car_bujia.vue";
import icon_car_engine from "./icon_car_engine.vue";
import icon_car_fuel from "./icon_car_fuel.vue";
import icon_car_oil from "./icon_car_oil.vue";
import icon_car_piston from "./icon_car_piston.vue";
import icon_car_speed from "./icon_car_speed.vue";
import icon_car_temp from "./icon_car_temp.vue";
import icon_car_tires from "./icon_car_tires.vue";
import icon_car_wifi from "./icon_car_wifi.vue";
import icon_cards from "./icon_cards.vue";
import icon_cel_level_0 from "./icon_cel_level_0.vue";
import icon_cel_level_1 from "./icon_cel_level_1.vue";
import icon_cel_level_2 from "./icon_cel_level_2.vue";
import icon_cel_level_3 from "./icon_cel_level_3.vue";
import icon_cel_level_4 from "./icon_cel_level_4.vue";
import icon_center from "./icon_center.vue";
import icon_certified from "./icon_certified.vue";
import icon_checkmark from "./icon_checkmark.vue";
import icon_close from "./icon_close.vue";
import icon_close_circle from "./icon_close_circle.vue";
import icon_consolidate from "./icon_consolidate.vue";
import icon_corporate from "./icon_corporate.vue";
import icon_crane from "./icon_crane.vue";
import icon_crane_big from "./icon_crane_big.vue";
import icon_desktop from "./icon_desktop.vue";
import icon_document from "./icon_document.vue";
import icon_document_download from "./icon_document_download.vue";
import icon_driver from "./icon_driver.vue";
import icon_drivers from "./icon_drivers.vue";
import icon_edit_pen from "./icon_edit_pen.vue";
import icon_ellipses from "./icon_ellipses.vue";
import icon_entidad_detail from "./icon_entidad_detail.vue";
import icon_envelope from "./icon_envelope.vue";
import icon_eventos from "./icon_eventos.vue";
import icon_expand_window from "./icon_expand_window.vue";
import icon_flecha_acc from "./icon_flecha_acc.vue";
import icon_flecha_acc_activa from "./icon_flecha_acc_activa.vue";
import icon_fleet_management from "./icon_fleet_management.vue";
import icon_geofences from "./icon_geofences.vue";
import icon_hamburger_menu from "./icon_hamburger_menu.vue";
import icon_help from "./icon_help.vue";
import icon_history from "./icon_history.vue";
import icon_home_panels from "./icon_home_panels.vue";
import icon_home from "./icon_home.vue";
import icon_horarios from "./icon_horarios.vue";
import icon_deactivate from "./icon_deactivate.vue";
import icon_info from "./icon_info.vue";
import icon_key from "./icon_key.vue";
import icon_label_fill from "./icon_label_fill.vue";
import icon_label_new from "./icon_label_new.vue";
import icon_label_outline from "./icon_label_outline.vue";
import icon_launcher from "./icon_launcher.vue";
import icon_layers from "./icon_layers.vue";
import icon_link from "./icon_link.vue";
import icon_lists from "./icon_lists.vue";
import icon_livetracking from "./icon_livetracking.vue";
import icon_lock from "./icon_lock.vue";
import icon_lock_open from "./icon_lock_open.vue";
import icon_lock_view from "./icon_lock_view.vue";
import icon_logout from "./icon_logout.vue";
import icon_maps from "./icon_maps.vue";
import icon_menu_opener from "./icon_menu_opener.vue";
import icon_mobile from "./icon_mobile.vue";
import icon_motorcycle from "./icon_motorcycle.vue";
import icon_new_user from "./icon_new_user.vue";
import icon_notifications from "./icon_notifications.vue";
import icon_pickup from "./icon_pickup.vue";
import icon_places from "./icon_places.vue";
import icon_question from "./icon_question.vue";
import icon_reloj from "./icon_reloj.vue";
import icon_reset from "./icon_reset.vue";
import icon_search from "./icon_search.vue";
import icon_settings from "./icon_settings.vue";
import icon_share from "./icon_share.vue";
import icon_start_point from "./icon_start_point.vue";
import icon_switch_maps from "./icon_switch_maps.vue";
import icon_toggle from "./icon_toggle.vue";
import icon_trails from "./icon_trails.vue";
import icon_truck from "./icon_truck.vue";
import icon_unlink from "./icon_unlink.vue";
import icon_user from "./icon_user.vue";
import icon_user_settings from "./icon_user_settings.vue";
import icon_van from "./icon_van.vue";
import icon_view_selector from "./icon_view_selector.vue";

export default {
  icon_account_settings,
  icon_add,
  icon_add_fill,
  icon_agents,
  icon_alarma,
  icon_analytics,
  icon_arrow_back,
  icon_arrow_course,
  icon_arrow_down,
  icon_arrow_drop_down,
  icon_arrow_drop_up,
  icon_arrow_forward,
  icon_arrow_up,
  icon_attention,
  icon_bag,
  icon_bell,
  icon_bookmark,
  icon_bookmark_outline,
  icon_brujula,
  icon_brujula_vacia,
  icon_calendar,
  icon_camera,
  icon_car,
  icon_car_battery,
  icon_car_brakes,
  icon_car_bujia,
  icon_car_engine,
  icon_car_fuel,
  icon_car_oil,
  icon_car_piston,
  icon_car_speed,
  icon_car_temp,
  icon_car_tires,
  icon_car_wifi,
  icon_cards,
  icon_cel_level_0,
  icon_cel_level_1,
  icon_cel_level_2,
  icon_cel_level_3,
  icon_cel_level_4,
  icon_center,
  icon_certified,
  icon_checkmark,
  icon_close,
  icon_close_circle,
  icon_consolidate,
  icon_corporate,
  icon_crane,
  icon_crane_big,
  icon_desktop,
  icon_document,
  icon_document_download,
  icon_driver,
  icon_drivers,
  icon_edit_pen,
  icon_ellipses,
  icon_entidad_detail,
  icon_envelope,
  icon_eventos,
  icon_expand_window,
  icon_flecha_acc,
  icon_flecha_acc_activa,
  icon_fleet_management,
  icon_geofences,
  icon_hamburger_menu,
  icon_help,
  icon_history,
  icon_home_panels,
  icon_home,
  icon_horarios,
  icon_deactivate,
  icon_info,
  icon_key,
  icon_label_fill,
  icon_label_new,
  icon_label_outline,
  icon_launcher,
  icon_layers,
  icon_link,
  icon_lists,
  icon_livetracking,
  icon_lock,
  icon_lock_open,
  icon_lock_view,
  icon_logout,
  icon_maps,
  icon_menu_opener,
  icon_mobile,
  icon_motorcycle,
  icon_new_user,
  icon_notifications,
  icon_pickup,
  icon_places,
  icon_question,
  icon_reloj,
  icon_reset,
  icon_search,
  icon_settings,
  icon_share,
  icon_start_point,
  icon_switch_maps,
  icon_toggle,
  icon_trails,
  icon_truck,
  icon_unlink,
  icon_user,
  icon_user_settings,
  icon_van,
  icon_view_selector,
};
